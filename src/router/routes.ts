import Home from '@/views/Home.vue';
import { RouteConfig } from 'vue-router';

const routes: RouteConfig[] = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      label: 'home'
    }
  }
];

export default routes;
