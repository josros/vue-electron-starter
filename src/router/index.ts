import Vue from 'vue';
import Router from 'vue-router';
import routes from './routes';

Vue.use(Router);

const router = new Router({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes
});

// router.beforeEach(
//   async (to, from, next): Promise<void> => {
//     next();
//   }
// );
// router.afterEach((to, from): void => {
// });

export default router;
