import App from '@/App.vue';
import i18n from '@/i18n';
import vuetify from '@/plugins/vuetify';
import router from '@/router';
import Vue, { VNode } from 'vue';

Vue.config.productionTip = false;

new Vue({
  vuetify,
  router,
  i18n,
  render: (h): VNode => h(App)
}).$mount('#app');
