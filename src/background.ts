'use strict';

import { app, BrowserWindow, globalShortcut, protocol } from 'electron';
import { createProtocol, installVueDevtools } from 'vue-cli-plugin-electron-builder/lib';

const isDevelopment = process.env.NODE_ENV !== 'production';

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win: BrowserWindow | null;

// Standard scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([{ scheme: 'app', privileges: { standard: true, secure: true } }]);

function createWindow(): void {
  win = new BrowserWindow({
    width: 1024,
    height: 768,
    webPreferences: {
      nodeIntegration: true
    }
  });
  win.setMinimumSize(200, 200);

  // will go back to the index page in case anything goes wrong (e.g. no internet during login)
  // here is a list of reasons: https://cs.chromium.org/chromium/src/net/base/net_error_list.h
  win.webContents.on('did-fail-load', (event, code, desc, url, isMainFrame): void => {
    // https://cs.chromium.org/chromium/src/net/base/net_error_list.h
    if (code !== -3) {
      // occurs during normal login flow
      console.warn('ELECTRON: DID FAIL LOAD: ', code, desc, url, isMainFrame);
      if (win && process.env.WEBPACK_DEV_SERVER_URL) {
        win.loadURL(process.env.WEBPACK_DEV_SERVER_URL as string);
      } else if (win) {
        win.loadURL('app://./index.html');
      }
    }
  });

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    win.loadURL(process.env.WEBPACK_DEV_SERVER_URL as string);
    if (!process.env.IS_TEST) {
      win.webContents.openDevTools();
    }
  } else {
    createProtocol('app');
    // Load the index.html when not in development
    win.loadURL('app://./index.html');
    globalShortcut.register('CommandOrControl+Alt+I', (): void => {
      if (win) {
        win.webContents.openDevTools();
      }
    });
  }

  win.on('closed', (): void => {
    win = null;
  });
}

app.on('will-quit', (): void => {
  globalShortcut.unregisterAll();
});

// Quit when all windows are closed.
app.on('window-all-closed', (): void => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', (): void => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow();
  }
});

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on(
  'ready',
  async (): Promise<void> => {
    if (isDevelopment && !process.env.IS_TEST) {
      // Install Vue Devtools
      try {
        await installVueDevtools();
      } catch (e) {
        console.error('Vue Devtools failed to install:', e.toString());
      }
    }
    createWindow();
  }
);

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', (data): void => {
      if (data === 'graceful-exit') {
        app.quit();
      }
    });
  } else {
    process.on('SIGTERM', (): void => {
      app.quit();
    });
  }
}
