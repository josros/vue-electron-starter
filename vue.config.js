module.exports = {
  // put version from package.json to process.env
  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  chainWebpack: (config) => {
    config.resolve.symlinks(false);
    config.plugin('define').tap((args) => {
      const v = JSON.stringify(require('./package.json').version);
      args[0]['process.env'].VERSION = v;
      return args;
    });
  },

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false
    },
    electronBuilder: {
      nodeIntegration: true
    }
  }
};
