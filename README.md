# vue-electron-starter

### Prerequisites

1. `node` and `npm` is installed
2. `npm install`

### Local Execution

```
npm run electron:serve
```

### Build for your local os

```
npm run electron:build
```

### Build for windows

```
npm run electron:build:win
```


### Maintainers

- Josef Rossa
