import HelloWorld from '@/components/HelloWorld.vue';
import { createLocalVue, shallowMount, Wrapper } from '@vue/test-utils';
import Vuetify from 'vuetify';

describe('HelloWorld.vue', (): void => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: Wrapper<any>;

  const localVue = createLocalVue();
  localVue.use(Vuetify);

  beforeEach((): void => {
    wrapper = shallowMount(HelloWorld, {
      localVue,
      mocks: {
        $t: (key: string): string => key
      }
    });
  });

  it('mounts', (): void => {
    expect(wrapper.html()).toContain('test');
  });
});
