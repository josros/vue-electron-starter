import { HelloWorldService } from '../../../src/services/helloWorld';

describe('HelloWorldService', () => {
  it('hello returns Hello World', async (): Promise<void> => {
    
    const service = new HelloWorldService();
    const result = service.hello();

    expect(result).toBe("Hello World");
  });
});
